#!/usr/bin/env node
const spawn = require('cross-spawn');
const utilties = require('../utilities');

const result = spawn.sync('webpack', [
  '--config', utilties.resolveConfigFile('webpack.config.js'),
  '-p'
], {stdio: 'inherit'});
process.exit(result.status);
