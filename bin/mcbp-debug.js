#!/usr/bin/env node
const spawn = require('cross-spawn');
const utilties = require('../utilities');

const result = spawn.sync('webpack-dev-server', [
  '--content-base', 'src',
  '--inline', '--hot',
  '--config', utilties.resolveConfigFile('webpack.config.js')
  ], {stdio: 'inherit'});
process.exit(result.status);
