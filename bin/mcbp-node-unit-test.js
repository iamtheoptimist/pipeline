#!/usr/bin/env node
const spawn = require('cross-spawn');
const utilties = require('../utilities');

const result = spawn.sync('mocha-webpack', [
  '--opts', utilties.resolveConfigFile('mocha-webpack.opts')
], {stdio: 'inherit'});
process.exit(result.status);
