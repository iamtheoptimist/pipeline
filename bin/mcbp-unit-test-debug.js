#!/usr/bin/env node
const spawn = require('cross-spawn');
const utilities = require('../utilities');

const result = spawn.sync('karma', ['start', utilities.resolveConfigFile('karma.conf.js')], {stdio: 'inherit'});
process.exit(result.status);
