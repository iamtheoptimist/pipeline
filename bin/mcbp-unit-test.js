#!/usr/bin/env node
const spawn = require('cross-spawn');
const utilties = require('../utilities');

const result = spawn.sync('karma', ['start', utilties.resolveConfigFile('karma.conf.prod.js')], {stdio: 'inherit'});
process.exit(result.status);
