// get source files (for coverage and reporting)
const sourceContext = require.context('../../../src', true, /\.jsx?$/);
sourceContext
  .keys()
  .forEach(sourceContext);