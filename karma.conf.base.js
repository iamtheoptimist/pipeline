const webpack = require('webpack');
const path = require('path');
const webpackConfig = require('./webpack.config');
const utilities = require('./utilities');

const testFiles = utilities.resolveConfigFile('karma-test-files.js');

module.exports = {
  basePath: path.join(__dirname, '../../..'),
  frameworks: [ 'mocha' ],
  files: [
    testFiles
  ],
  preprocessors: {
    [testFiles]: ['webpack', 'sourcemap']
  },
  webpack: {
    devtool: 'inline-source-map',
    module: webpackConfig.module,
    plugins: webpackConfig.plugins,
    externals: {
      'cheerio': 'window',
      'sinon': true,
      'react/addons': true,
      'react/lib/ExecutionEnvironment': true,
      'react/lib/ReactContext': true
    },
    watch: true
  },
  reporters: ['mocha', 'junit', 'coverage'],
  junitReporter: {
    outputDir: 'reports/prod/test-reports',
    outputFile: 'test-report.xml',
    suite: '', // suite will become the package name attribute in xml testsuite element
    useBrowserName: false, // add browser name to report and classes names
    nameFormatter: undefined, // function (browser, result) to customize the name attribute in xml testcase element
    classNameFormatter: undefined, // function (browser, result) to customize the classname attribute in xml testcase element
    properties: {} // key value pair of properties to add to the <properties> section of the report
  },
  coverageReporter: {
    reporters: [
      { type: 'cobertura', dir: 'reports/prod/test-reports', subdir: '.'},
      { type: 'text-summary' }
    ]
  },
  webpackServer: {
    noInfo: true,
    stats: 'errors-only'
  },
  browsers: [ 'PhantomJS' ],
  port: 9876,
  colors: true,
  autoWatch: true,
  singleRun: true
}