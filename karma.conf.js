const baseConfig = require('./karma.conf.base');

process.env.NODE_ENV = 'test';

module.exports = function (config) {
  config.set(Object.assign({}, baseConfig, {
    reporters: ['mocha', 'coverage'],
    coverageReporter: {
      reporters: [
        { type: 'html', dir: 'reports/' },
        { type: 'text-summary' }
      ]
    },
    browsers: [ 'Chrome' ],
    singleRun: false
  }));
};
