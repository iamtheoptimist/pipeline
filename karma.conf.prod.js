const baseConfig = require('./karma.conf.base');

process.env.NODE_ENV = 'test';

module.exports = function (config) {
  config.set(baseConfig);
};
