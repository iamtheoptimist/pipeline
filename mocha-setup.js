require('source-map-support').install({
  handleUncaughtExceptions: false,
  environment: 'node',
});

const { JSDOM } = require('jsdom');
const dom = new JSDOM();
global.document = dom.window.document;
global.window = dom.window;
window.console = global.console;
global.WebSocket = require('ws');

Object.keys(document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    global[property] = document.defaultView[property];
  }
});

global.navigator = {
  userAgent: 'node.js'
};
