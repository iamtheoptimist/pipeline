const nodeExternals = require('webpack-node-externals');
const pipelineConfig = require('./webpack.config');

module.exports = Object.assign({}, pipelineConfig, {
  target: 'node',
  externals: [nodeExternals()],
  devtool: "#inline-cheap-module-source-map",
  output: Object.assign({}, pipelineConfig.options, {
    devtoolModuleFilenameTemplate: '[absolute-resource-path]',
    devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]'
  }),
});
