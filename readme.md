# MCB Pipeline

MCB Pipeline is a configuration accelerator aimed at getting projects up and running very quickly.

It configures other great libraries to make development easier for consumers.

## What it gives you

MCB Pipeline exposes a number of npm commands, which can enable a project for "build, test and run" without the need for complex configuration or boilerplate code.

The idea is to allow consumers to focus on their code rather than on the mechanics of building, debugging and testing that code.

With a few commands the pipeline will:

- **Allow the use of ES2015 JavaScript ([Webpack](https://webpack.github.io/), [Babel](https://babeljs.io/))**
- **Run and debug unit tests ([Webpack](https://webpack.github.io/), [Babel](https://babeljs.io/), [Karma](https://karma-runner.github.io/1.0/index.html) and [Mocha](https://mochajs.org/) assertions)**
- **Allow the use of ES2015 JavaScript in unit tests ([Webpack](https://webpack.github.io/), [Babel](https://babeljs.io/), [Karma](https://karma-runner.github.io/1.0/index.html) and [Mocha](https://mochajs.org/) assertions)**
- **Support live debugging and automatic module reloading ([Webpack](https://webpack.github.io/))**
- **Allow the use of testing methods (describe, it, etc) ([Mocha](https://mochajs.org/))**
- **Allow the use of the Less preprocessor ([Less](http://lesscss.org/))**
- **Offer a proxy server for data services ([Webpack](https://webpack.github.io/))**

## Principals

The pipeline uses various libraries behind the scenes to build and test a project (see above). Generally speaking, the pipeline should be considered a black box. It configures other libraries to make it easier for you. In this respect consumers should not be too concerned with the 'how', just that the pipeline should always support the functionality listed above.

All supporting libraries are bundled, meaning they will be automatically installed when `@mcb/pipeline` installs. For more details on the specific libraries that the pipeline uses, please see the commands below and the npm dependencies.

## Installation

In the target project run:

```
npm install @mcb/pipeline
```

## Use

MCB Pipeline exposes a number of commands that you may use in npm scripts to facilitate your needs.

### Available commands

#### `mcbp-build`

This command will build a production ready version of the project into the `dist` directory. The command will uglify and optimise the bundle for distribution.

**Please note**, at present, it will expect to build the project from entry point `./src/index.js`.

#### `mcbp-debug`

**Prerequisite:** Google Chrome

This command will build and host a live version of the application for debugging. The bundle will be built with source maps for use in the browser. Source files will be hot loaded onto the server so that when you make changes, the bundle will be automatically rebuilt and the browser refreshed with your changes.

**Please note**, at present, like `mcbp-build`, it will expect to build the project from entry point `./src/index.js`.

#### `mcbp-unit-test`

This command will execute a single unit test sweep of your code, perfrom a coverage scan and report all findings.

Tests will be sought in the following locations:

- ./test (all .js files)
- ./src (all files prefixed with .spec.js or -spec.js)

#### `mcbp-unit-test-debug`

**Prerequisite:** Google Chrome

This command will execute a live debugging session, for all located unit tests, to facilitate debugging.

Tests will be sought in the following locations:

- ./test (all .js files)
- ./src (all files prefixed with .spec.js or -spec.js)

## Example use

The idea of MCB Pipeline is to simplify development and put emphasis on code and testing rather than configuration and boilerplate code.

The following shows the scripts section from a package.json file to demonstrate how a consumer might use MCB Pipeline:

```
"scripts": {
    "start": "mcbp-debug",
    "build": "mcbp-build",
    "unit": "mcbp-unit-test",
    "unit:debug": "mcbp-unit-test-debug"
}
```
