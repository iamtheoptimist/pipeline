const fs = require('fs');

function resolveConfigFile(configFilename) {
  const exists = (fs.existsSync(`./${configFilename}`));
  const configPath = exists ? `./${configFilename}` : `./node_modules/@mcb/pipeline/${configFilename}`;
  console.log(exists ?
    `overriding pipeline with local project file './${configPath}'` :
    `using pipeline config ${configPath}`);
  return configPath;
}

module.exports = {
  resolveConfigFile
};
