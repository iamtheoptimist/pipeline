const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const utilities = require('./utilities');
const babelConfig = JSON.parse(fs.readFileSync(utilities.resolveConfigFile('.babelrc'), 'utf8'));

module.exports = {

  context: path.join(__dirname, '../../..'),

  entry: {
    app: './src/index'
  },

  output: {
    path: path.resolve('./dist'),
    filename: '[name].[hash].js'
  },

  resolve: {
    extensions: [".js", ".jsx"]
  },

  module: {
    rules: [
      { test: /\.jsx?$/, exclude: [ /node_modules/ ], use: [{ loader: 'babel-loader', options: babelConfig }]},
      { test: /\.css$/, use: ['style-loader', 'css-loader' ] },
      { test: /\.less$/, loader: ExtractTextPlugin.extract({ fallback: "style-loader", loader: ["css-loader", "less-loader"] }) },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: [{ loader: 'url-loader?limit=10000&mimetype=application/font-woff' }] },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: [{ loader: 'file-loader' }] },
      { test: /\.json$/, exclude: /(node_modules)/, use: [{ loader: 'json-loader' }] }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, '../../../src/index.html'),
      filename: 'index.html',
      js: [ '[name].[hash].js' ],
      chunks: {
        'head': {
          'css': '[name].[hash].css'
        },
        'main': {
          'entry': '[name].[hash].js'
        },
      }
    }),
    new ExtractTextPlugin({ filename: '[name].[hash].css', disable: false, allChunks: true })
  ]
};
